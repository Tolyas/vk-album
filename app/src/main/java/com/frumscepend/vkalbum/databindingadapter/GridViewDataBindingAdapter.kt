package com.frumscepend.vkalbum.databindingadapter

import android.databinding.BindingMethod
import android.databinding.BindingMethods
import android.widget.GridView


@BindingMethods(
        BindingMethod(type = GridView::class, attribute = "app:adapter", method = "setAdapter"),
        BindingMethod(type = GridView::class, attribute = "app:item_click_listener", method = "setOnItemClickListener")
)
class GridViewDataBindingAdapter private constructor() {

    init {
        throw UnsupportedOperationException()
    }
}