package com.frumscepend.vkalbum.databindingadapter

import android.databinding.BindingMethod
import android.databinding.BindingMethods
import android.support.v4.view.ViewPager


@BindingMethods(
        BindingMethod(type = ViewPager::class, attribute = "app:offscreenPageLimit", method = "setOffscreenPageLimit"),
        BindingMethod(type = ViewPager::class, attribute = "app:adapter", method = "setAdapter"),
        BindingMethod(type = ViewPager::class, attribute = "app:currentPage", method = "setCurrentItem")
)
class ViewPagerDataBindingAdapter private constructor() {

    init {
        throw UnsupportedOperationException()
    }
}