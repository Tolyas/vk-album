package com.frumscepend.vkalbum.databindingadapter

import android.databinding.BindingMethod
import android.databinding.BindingMethods
import android.widget.ImageView


@BindingMethods(
        BindingMethod(type = ImageView::class, attribute = "app:bitmap", method = "setImageBitmap")
)
class ImageViewDataBindingAdapter private constructor() {

    init {
        throw UnsupportedOperationException()
    }
}