package com.frumscepend.vkalbum.data.source.remote

import android.graphics.Bitmap
import com.frumscepend.vkalbum.data.Model
import com.frumscepend.vkalbum.data.Response
import com.frumscepend.vkalbum.data.source.DataNotAvailableException
import com.frumscepend.vkalbum.data.source.VKPhotoDataSource
import com.frumscepend.vkalbum.utils.ImageLoader
import com.google.gson.Gson
import com.vk.sdk.api.VKError
import com.vk.sdk.api.VKRequest
import com.vk.sdk.api.VKResponse
import io.reactivex.Observable


class VKPhotoRemoteDataSource private constructor(): VKPhotoDataSource{
    override fun getPhotos(): Observable<Response> {
        return Observable.create { subscriber ->
            val request = VKRequest("photos.getAll")
            request.executeWithListener(object : VKRequest.VKRequestListener() {
                override fun onComplete(response: VKResponse?) {
                    subscriber.onNext(Gson().fromJson(response?.responseString, Model::class.java).response)
                    subscriber.onComplete()
                }

                override fun onError(error: VKError?) {
                    subscriber.onError(DataNotAvailableException())
                }
            })
        }
    }

    override fun getPhoto(url: String): Observable<Bitmap> {
        return Observable.create { subscriber ->
            val bmp = ImageLoader.LoadBitmapFromUrl(url)
            bmp?.let {
                subscriber.onNext(it)
                subscriber.onComplete()
            } ?: run {
                subscriber.onError(DataNotAvailableException())
            }
        }
    }

    companion object {

        private var INSTANCE: VKPhotoRemoteDataSource? = null

        /**
         * Returns the single instance of this class, creating it if necessary.
         * *
         * @return the [VKPhotoRemoteDataSource] instance
         */
        @JvmStatic fun getInstance() =
                INSTANCE ?: synchronized(VKPhotoRemoteDataSource::class.java) {
                    INSTANCE ?: VKPhotoRemoteDataSource()
                            .also { INSTANCE = it }
                }


        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }

}