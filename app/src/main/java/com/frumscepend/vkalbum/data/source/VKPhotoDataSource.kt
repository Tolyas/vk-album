package com.frumscepend.vkalbum.data.source

import android.graphics.Bitmap
import com.frumscepend.vkalbum.data.Response
import io.reactivex.Observable

interface VKPhotoDataSource {

    fun getPhotos(): Observable<Response>

    fun getPhoto(url: String): Observable<Bitmap>
}