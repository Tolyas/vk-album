package com.frumscepend.vkalbum.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Response {

    @SerializedName("count")
    @Expose
    var count: Int = 0
    @SerializedName("items")
    @Expose
    var photos: List<Photo>? = null

}
