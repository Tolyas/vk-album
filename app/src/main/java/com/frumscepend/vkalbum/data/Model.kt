package com.frumscepend.vkalbum.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Model {

    @SerializedName("response")
    @Expose
    var response: Response = Response()

}