package com.frumscepend.vkalbum

import com.frumscepend.vkalbum.data.source.VKPhotoDataSource
import com.frumscepend.vkalbum.data.source.VKPhotoRepository
import com.frumscepend.vkalbum.data.source.cache.VKPhotoCacheDataSource
import com.frumscepend.vkalbum.data.source.remote.VKPhotoRemoteDataSource

/**
 * Enables injection of any required data sources or mock it for testing
 */

object Injection {

    fun provideVKPhotoRepository(): VKPhotoDataSource {
        return VKPhotoRepository.getInstance(VKPhotoCacheDataSource.getInstance(),
                VKPhotoRemoteDataSource.getInstance())
    }
}
