package com.frumscepend.vkalbum.main.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.frumscepend.vkalbum.R
import com.frumscepend.vkalbum.data.Photo
import com.frumscepend.vkalbum.databinding.FragmentImageDetailsBinding
import com.frumscepend.vkalbum.main.activity.PhotoActivity
import com.frumscepend.vkalbum.main.viewmodel.PhotoDetailsViewModel
import com.frumscepend.vkalbum.utils.obtainViewModel

class PhotoDetailsFragment: Fragment() {
    private lateinit var fragmentImageDetailsBinding: FragmentImageDetailsBinding
    var photo = Photo()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentImageDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_image_details, container, false)
        fragmentImageDetailsBinding.viewmodel = (activity as PhotoActivity).obtainViewModel(photo.id.toString(), PhotoDetailsViewModel::class.java)
        fragmentImageDetailsBinding.viewmodel?.let { model ->
            model.photo = photo
        }
        return fragmentImageDetailsBinding.root
    }

    override fun onResume() {
        super.onResume()
        fragmentImageDetailsBinding.viewmodel?.loadPhoto()
    }

    companion object {
        fun newInstance() = PhotoDetailsFragment()
    }
}