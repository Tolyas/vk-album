package com.frumscepend.vkalbum.main.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableInt
import android.databinding.ObservableList
import com.frumscepend.vkalbum.data.Photo
import com.frumscepend.vkalbum.data.source.VKPhotoDataSource
import com.frumscepend.vkalbum.main.adapter.PhotosAdapter

class PhotoViewModel(
        context: Application,
        private val vkPhotoRepository: VKPhotoDataSource
): AndroidViewModel(context) {
    var position = ObservableInt(0)
    val fail = ObservableBoolean(false)
    val photos: ObservableList<Photo> = ObservableArrayList()
    lateinit var photosAdapter: PhotosAdapter

    /**
     * Loading pictures
     */
    fun loadPictures(){
        vkPhotoRepository.getPhotos()
                .subscribe({response ->
                    fail.set(false)
                    photos.clear()
                    response.photos?.let { photos.addAll(it) }
                    photosAdapter.notifyDataSetChanged()
                    position.notifyChange()
                },{
                    fail.set(true)
                })
    }


}