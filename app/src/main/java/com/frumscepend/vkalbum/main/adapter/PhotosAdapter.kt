package com.frumscepend.vkalbum.main.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import com.frumscepend.vkalbum.main.fragment.PhotoDetailsFragment
import com.frumscepend.vkalbum.main.viewmodel.PhotoViewModel

class PhotosAdapter (
        private val viewModel: PhotoViewModel,
        fm: FragmentManager
): FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val photoDetailsFragment = PhotoDetailsFragment.newInstance()
        photoDetailsFragment.photo = viewModel.photos[position]
        return photoDetailsFragment
    }

    override fun getCount(): Int {
        return viewModel.photos.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }
}