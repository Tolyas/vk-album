package com.frumscepend.vkalbum.main.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.widget.AdapterView
import com.frumscepend.vkalbum.data.source.VKPhotoDataSource
import com.frumscepend.vkalbum.main.adapter.PhotosGridAdapter

class MainViewModel(
        context: Application,
        private val vkPhotoRepository: VKPhotoDataSource
): AndroidViewModel(context) {
    val fail = ObservableBoolean(false)
    val thumbnailAdapter = PhotosGridAdapter(vkPhotoRepository)
    lateinit var onItemClickListener: AdapterView.OnItemClickListener

    /**
     * Loading pictures
     */
    fun loadPictures(){
        vkPhotoRepository.getPhotos()
                .subscribe({response ->
                    fail.set(false)
                    response.photos?.let { photos ->
                        thumbnailAdapter.photos.clear()
                        thumbnailAdapter.photos.addAll(photos)
                        thumbnailAdapter.notifyDataSetChanged()
                    }
                },{
                    fail.set(true)
                })
    }

}